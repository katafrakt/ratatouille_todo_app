defmodule TaskApp.Database do
  use Agent

  def start_link(opts) do
    Agent.start_link(
      fn ->
        %{
          tasks:
            [
              %{name: "Buy broccoli"},
              %{name: "Call mom"},
              %{name: "Clean up the mess in TaskApp code"}
            ]
            |> Enum.map(fn task ->
              Map.merge(task, %{completed?: false, log: [{:created, DateTime.utc_now()}]})
            end)
        }
      end,
      opts
    )
  end

  def tasks(pid) do
    Agent.get(pid, & &1.tasks)
  end

  def delete_task(pid, task) do
    Agent.update(pid, fn state ->
      tasks = Enum.reject(state.tasks, &(&1 == task))
      %{state | tasks: tasks}
    end)
  end

  def add_task(pid, name) do
    task = %{name: name, completed?: false, log: [{:created, DateTime.utc_now()}]}

    Agent.update(pid, fn state ->
      %{state | tasks: state.tasks ++ [task]}
    end)
  end

  def toggle_completion(pid, highlighted) do
    Agent.update(pid, fn state ->
      tasks =
        state.tasks
        |> Enum.map(fn task ->
          if task == highlighted do
            completed? = task.completed?
            event = if completed?, do: :uncompleted, else: :completed
            log = [{event, DateTime.utc_now()} | task.log]
            %{task | completed?: !completed?, log: log}
          else
            task
          end
        end)

      %{state | tasks: tasks}
    end)
  end
end
