defmodule TodoApp.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    ratatuille_opts = [
      app: TodoApp,
      shutdown: {:application, :todo_app}
    ]

    children = [
      # Starts a worker by calling: TodoApp.Worker.start_link(arg)
      # {TodoApp.Worker, arg}
      {Ratatouille.Runtime.Supervisor, runtime: ratatuille_opts}
      # {TaskApp.Database, name: TaskApp.Repo}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TodoApp.Supervisor]
    Supervisor.start_link(children, opts)
  end

  @impl true
  def stop(_state) do
    System.halt()
  end
end
