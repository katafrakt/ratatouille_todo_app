defmodule TodoApp do
  @behaviour Ratatouille.App

  import Ratatouille.View
  alias TaskApp.Database

  import Ratatouille.Constants, only: [key: 1, color: 1]

  @arrow_up key(:arrow_up)
  @arrow_down key(:arrow_down)
  @space key(:space)
  @backspace key(:backspace2)
  @esc key(:esc)
  @enter key(:enter)

  @style_row_selected [
    color: color(:black),
    background: color(:white)
  ]

  @impl true
  def init(%{window: window}) do
    {:ok, db_pid} = Database.start_link([])
    tasks = Database.tasks(db_pid)
    %{tasks: tasks, cursor: 0, db: db_pid, window: window, mode: :list, new_task_name: ""}
  end

  @impl true
  def update(model, message) do
    Map.put(model, :message, message)

    case {model, message} do
      {%{mode: :list}, {:event, %{key: key}}} when key == @arrow_down ->
        if model.cursor < length(model.tasks) - 1,
          do: %{model | cursor: model.cursor + 1},
          else: model

      {%{mode: :list}, {:event, %{key: key}}} when key == @arrow_up ->
        if model.cursor > 0,
          do: %{model | cursor: model.cursor - 1},
          else: model

      {%{mode: :list}, {:event, %{ch: ch}}} when ch == ?d ->
        current = Enum.at(model.tasks, model.cursor)
        Database.delete_task(model.db, current)
        cursor = if model.cursor == 0, do: 0, else: model.cursor - 1
        %{model | tasks: Database.tasks(model.db), cursor: cursor}

      {%{mode: :list}, {:event, %{ch: ch}}} when ch == ?c ->
        current = Enum.at(model.tasks, model.cursor)
        Database.toggle_completion(model.db, current)
        %{model | tasks: Database.tasks(model.db)}

      {%{mode: :list}, {:event, %{ch: ch}}} when ch == ?n ->
        %{model | mode: :new}

      {%{mode: :new}, {:event, %{ch: ch}}} when ch != 0 ->
        task_name = model.new_task_name <> <<ch::utf8>>
        %{model | new_task_name: task_name}

      {%{mode: :new}, {:event, %{key: key}}} when key == @space ->
        task_name = model.new_task_name <> " "
        %{model | new_task_name: task_name}

      {%{mode: :new}, {:event, %{key: key}}} when key == @backspace ->
        task_name = String.slice(model.new_task_name, 0..-2)
        %{model | new_task_name: task_name}

      {%{mode: :new}, {:event, %{key: key}}} when key == @enter ->
        Database.add_task(model.db, model.new_task_name)
        %{model | tasks: Database.tasks(model.db), mode: :list}

      {%{mode: :new}, {:event, %{key: key}}} when key == @esc ->
        %{model | mode: :list}

      _ ->
        model
    end
  end

  @impl true
  def render(model) do
    highlighted_task = Enum.at(model.tasks, model.cursor)

    view do
      row do
        column(size: 6) do
          panel(title: "Tasks", height: :fill) do
            table do
              for task <- model.tasks do
                table_row(if task == highlighted_task, do: @style_row_selected, else: []) do
                  table_cell(
                    content: "[#{if task[:completed?], do: "✓", else: " "}] " <> task.name
                  )
                end
              end
            end
          end
        end

        column(size: 6) do
          row do
            column(size: 12) do
              panel(title: "Details", height: model.window.height - 10) do
                label(content: highlighted_task.name)
                label(content: "")

                for {event, time} <- highlighted_task.log do
                  label(content: "[#{time.hour}:#{time.minute}:#{time.second}] #{event}")
                end
              end
            end
          end

          row do
            column(size: 12) do
              help(model.mode)
            end
          end
        end
      end

      add_new_window(model)
    end
  end

  defp help(:list) do
    panel(title: "Help", height: 10) do
      label(content: "n - add new todo")
      label(content: "c - toggle completion")
      label(content: "d - delete todo")
    end
  end

  defp help(:new) do
    panel(title: "Help", height: 10) do
      label(content: "enter - add todo")
      label(content: "esc - cancel")
    end
  end

  defp add_new_window(%{mode: :new} = model) do
    overlay(padding: 10) do
      panel(title: "Add new task", height: :fill) do
        label(content: model.new_task_name)
      end
    end
  end

  defp add_new_window(_), do: nil
end
